<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_contoh extends CI_Model {
	
	public $field1 = "";
	public $field2 = "";
	
	public function get_data(){
		$query = "
			SELECT *
			FROM tabel
			ORDER BY kode DESC
		";
		return $this->db->query($query)->result();
	}
	
	public function by_id($id){
		$datasrc = $this->db->get_where('tabel', array('kode' => $id));
		return $datasrc->num_rows() > 0 ? $datasrc->row() : $this;
	}
	
}
/* End of file Model_contoh.php */
/* Location: ./application/modules/master/models/Model_contoh.php */