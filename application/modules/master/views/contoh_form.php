<div class="col-md-12 col-sm-12">
	
	<div class="card">
        <div class="card-header">
            <h4 class="card-title m-b-0">Form Contoh</h4>
            <a href="<?php echo $back; ?>"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</a>
        </div>
        <div class="card-body">
            <!-- form start -->
            <?php echo form_open_multipart($action, array('class' => 'form-horizontal row-form')); ?>
              <div class="col-md-12">
              	<div class="form-group">
                  <label for="field_1">Field 1</label>
                  <input type="text" class="form-control" id="field_1" name="field_1" value="<?php echo $contoh->field1; ?>">
                </div>
                <div class="form-group">
                  <label for="field_2">Field 2</label>
                  <input type="text" class="form-control" id="field_2" name="field_2" value="<?php echo $contoh->field2; ?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="reset" class="btn btn-danger">Batal</button>
              </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>